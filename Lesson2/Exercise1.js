function makeObjectDeepCopy(value) {

  if(typeof value !== "object"){
    return value;
  }

  if(Array.isArray(value)) {
    return value.map(makeObjectDeepCopy);
  }

  const copiedObject = {};

  for(const key in value) {
    copiedObject[key] = makeObjectDeepCopy(value[key]);
  }

  return copiedObject;

}
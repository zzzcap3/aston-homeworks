const NOT_ONLY_NUMBERS_ARRAY_MESSAGE = "Array must include only number type",
      INTERVAL_ARGUMENTS_MUSTBE_NUMBER_ERROR = "Interval arguments must be number type";

const isNumber = (value) => {
  // проверяем так же на []; !isNaN([]) === true
  return typeof value === "number" && !isNaN(value);
};

const isArgumentsNumber = (arr) => {
  return arr.every(isNumber);
};

function selectFromInterval(arr, firstInterval, secondInterval) {
  if (!isArgumentsNumber(arr)) {
    throw new Error(NOT_ONLY_NUMBERS_ARRAY_MESSAGE);
  }

  if (!isNumber(firstInterval) || !isNumber(secondInterval)) {
    throw new Error(INTERVAL_ARGUMENTS_MUSTBE_NUMBER_ERROR);
  }

  if (firstInterval > secondInterval) {
    const fristIntervalValue = firstInterval;
    firstInterval = secondInterval;
    secondInterval = fristIntervalValue;
  }

  firstInterval = firstInterval <= 0 ? 0 : firstInterval - 1;

  return arr.slice(firstInterval, secondInterval);
}
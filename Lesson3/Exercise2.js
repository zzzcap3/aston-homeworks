function createDebounceFunction(callback, ms) {
    let isReady = false;
    let readyTimeout;
  
    return function fn(...args) {
        if (isReady) {

            callback.apply(this, args);
    
            isReady = false;
    
            readyTimeout = setTimeout(() => {
                isReady = true;
            }, ms);

        } else {

            clearTimeout(readyTimeout);
    
            readyTimeout = setTimeout(() => {
                callback.apply(this, args);

                isReady = false;
            }, ms);

        }
    }
}
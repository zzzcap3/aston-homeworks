(function () {
    if (!Array.prototype.hasOwnProperty("myFilter")) {

      function myFilter(callback) {
        const newArr = [];
  
        for (let i = 0; i < this.length; i++) {
          if (callback(this[i], i, this)) {
            newArr.push(this[i]);
          }
        }
  
        return newArr;
      }
  
      Object.defineProperty(Array.prototype, "myFilter", {
        value: myFilter
      })
      
    }
})();
function concatStrings(value, separator = "") {
    if (typeof value !== "string") {
      return "";
    }
  
    let result = value;
    separator = typeof separator === "string" ? separator : "";
  
    return function (arg) {
      if ((!arg && typeof arg !== "string") || typeof arg !== "string") {
        return result;
      }
  
      result += `${separator}${arg}`;
  
      return concatStrings.call(this, result, separator);
    };
}
  
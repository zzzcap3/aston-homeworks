const isValidNum = (num) => Number.isFinite(num);
const isValidArguments = (...args) => args.every(isValidNum);

class Calculator {
    static errors = {
      notANumber: 'One of param is not a number',
      divValidateY: 'Y cannot be 0',
      notValidArgument: "Not valid argument"
    };
  
    constructor(x, y) {
      if (!isValidArguments(x,y)) {
        throw new Error(Calculator.errors.notANumber);
      }
  
      this._x = x;
      this._y = y;
      this.logSum = this.logSum.bind(this);
      this.logMul = this.logMul.bind(this);
      this.logSub = this.logSub.bind(this);
      this.logDiv = this.logDiv.bind(this);
    }
  
    setX(num) {
      if (isValidNum(num)) {
        this._x = num;
      } else {
        throw new Error(Calculator.errors.notValidArgument);
      }
    }
  
    setY(num) {
      if (isValidNum(num)) {
        this._y = num;
      } else {
        throw new Error(Calculator.errors.notValidArgument);
      }
    }

    logSum() {
      console.log(this._x + this._y); 
    }

    logMul() {
      console.log(this._x * this._y); 
    }

    logSub() {
      console.log(this._x - this._y);
    }

    logDiv() {
      if(this._y === 0) {
        throw new Error(Calculator.errors.divValidateY);
      }

      console.log(this._x / this._y); 
    }
}
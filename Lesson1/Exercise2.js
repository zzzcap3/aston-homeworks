// utilities

const PROMPT_MESSAGE = "Пожалуйста введите значение";
const INCORRECT_VALUE_ERROR = "Некорректный ввод!";

const isNumber = (num) => {
    return !isNaN(num);
}

function isValidNumberArguments(...args) {
    return args.every(isNumber)
}

function getPromptData(message) {
    return window.prompt(message, "");
}

const sum = (...args) => args.reduce((acc,val) => acc + val);
const multi = (...args) => args.reduce((acc, val) => acc * val);

const answerTemplate = (sum, multi) => `Ответ: ${sum} ${multi}`;

// utilities end

window.addEventListener("DOMContentLoaded", () => {
    const firstArg = parseInt(getPromptData(PROMPT_MESSAGE)),
          secondArg = parseInt(getPromptData(PROMPT_MESSAGE));

    if(isValidNumberArguments(firstArg, secondArg)) {

        const argsSum = sum(firstArg, secondArg);
        const argsMulti = multi(firstArg, secondArg);

        console.log(answerTemplate(argsSum, argsMulti));

    } else {
        console.log(INCORRECT_VALUE_ERROR);
    }

})
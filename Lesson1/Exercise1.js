// utilities

const PROMPT_MESSAGE = "Пожалуйста введите значение";
const INCORRECT_VALUE_ERROR = "Некорректный ввод!";

const numberSystemCurry = (system) => (number) => parseInt(number.toString(system));

const isNumber = (num) => {
    return !isNaN(num);
}

function isValidNumberArguments(...args) {
    return args.every(isNumber)
}

function getPromptData(message) {
    return prompt(message, "");
}

// utilities end


const numberSystemFns = {
    // скрыли за абстракцией чтобы можно было легко поменять реализацию
    toBinary: numberSystemCurry(2),
    toOctal: numberSystemCurry(8),
    toDecimal: numberSystemCurry(10),
    toHexadecimal: numberSystemCurry(16)
}

const numberSystemsEnum = {
    "2": numberSystemFns.toBinary,
    "8": numberSystemFns.toOctal,
    "10": numberSystemFns.toDecimal,
    "16": numberSystemFns.toHexadecimal
}

function isValidNumberSystem(num) {
    return Object.keys(numberSystemsEnum).includes(num.toString());
}

window.addEventListener("DOMContentLoaded", () => {
    const number = parseInt(getPromptData(PROMPT_MESSAGE)),
          numberSystem = parseInt(getPromptData(PROMPT_MESSAGE));

    if(isValidNumberArguments(number, numberSystem) && isValidNumberSystem(numberSystem)) {

        const toNumberSystemFn = numberSystemsEnum[numberSystem];
        console.log(toNumberSystemFn(number));

    } else {
        console.log(INCORRECT_VALUE_ERROR);
    }

})